#include <iostream>
#include <boost/random/random_device.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/random/uniform_int_distribution.hpp>
using namespace std;
using namespace boost;


string getRandString(size_t n) {
	string line;
	string chars("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890 ,.");
    boost::random::random_device rng;
    boost::random::uniform_int_distribution<> index_dist(0, chars.size() - 1);
    for(int i = 0; i < n; ++i) {
        line += chars[index_dist(rng)];
    }
    return line;
	
}

void log(const string& s) {
    cout << s << endl;
}

string perform_algo1(const string& inp) {
	return to_upper_copy(inp);
}

string perform_algo2(const string& inp) {
	return to_lower_copy(inp);
}

string perform_algo3(const string& inp) {
	return trim_copy(inp);
}

string perform_algo4(const string& inp) {
	return trim_copy_if(inp, is_any_of("A"));
}

string perform_algo5(const string& inp) {
	return replace_first_copy(inp, "A", "&");
}

string perform_algo6(const string& inp) {
	return erase_first_copy(inp, "A");
}

string perform_algo7(const string& inp) {
	return replace_last_copy(inp, "A", "&");
}

string perform_algo8(const string& inp) {
	return erase_last_copy(inp, "A");
}

string perform_algo9(const string& inp) {
	return replace_nth_copy(inp, "A", 3, "&");
}

string perform_algo10(const string& inp) {
	return erase_nth_copy(inp, "A", 3);
}

string perform_algo11(const string& inp) {
	return replace_all_copy(inp, "A", "&");
}

string perform_algo12(const string& inp) {
	return erase_all_copy(inp, "A");
}

int main() {
	string str = getRandString(150);
	log("[default]:         \t"+str);
	log("to_upper_copy:     \t"+perform_algo1(str));
	log("to_lower_copy:     \t"+perform_algo2(str));
	log("trim_copy:         \t"+perform_algo3(" "+str+" "));
	log("trim_copy_if:      \t"+perform_algo4("A"+str+"A"));
	log("replace_first_copy:\t"+perform_algo5(str));
	log("erase_first_copy:  \t"+perform_algo6(str));
	log("replace_last_copy: \t"+perform_algo7(str));
	log("erase_last_copy:   \t"+perform_algo8(str));
	log("replace_nth_copy:  \t"+perform_algo9(str));
	log("erase_nth_copy:    \t"+perform_algo10(str));
	log("replace_all_copy:  \t"+perform_algo11(str));
	log("erase_all_copy:    \t"+perform_algo12(str));
	return 0;
}
