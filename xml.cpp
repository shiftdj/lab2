#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <boost/algorithm/string.hpp>
using namespace std;
using namespace boost;



class xml {
	string _name;
	size_t _level;
	vector<xml> _sub;
public:
	xml() : _name(), _level(0), _sub() {
		
	}
	xml(const xml& other) : _name(other._name), _level(other._level), _sub() {
		for(auto x: other._sub)
			_sub.push_back(x);
	}
	xml(string name, size_t level = 0) : _name(name), _level(level), _sub() {
		
	}
	xml(string name, string buffer, size_t level = 0) : _name(name), _level(level), _sub() {
		do {
			auto tag_b = find_nth(buffer, "<", 0);
			auto tag_e = find_nth(buffer, ">", 0);
			if(tag_b==tag_e) {
				trim(buffer);
				_sub.push_back(xml(buffer, _level+1)); 
				break;
			}
			string tag(++tag_b.begin(), tag_e.begin());
			auto tag_c = find_nth(buffer, "</"+tag+">", 0);
			_sub.push_back(xml(tag, string(++tag_e.begin(), tag_c.begin()), _level+1));
			buffer = string(tag_c.end(), buffer.end());
		} while(buffer != "");
	}
	
	void print() {
		for(size_t i = 0; i < _level; ++i)
			cout << '\t';
		if(!_sub.size()) {
			cout << _name << endl;
			return;
		}
		cout << "<" << _name << ">" << endl;
		for(auto x : _sub)
			x.print();
		for(size_t i = 0; i < _level; ++i)
			cout << '\t';
		cout << "</" << _name << ">" <<  endl; 
	}
};


xml parse(string fname) {
	ifstream file(fname.c_str());
	if(!file.is_open()) 
		throw runtime_error("Can't open "+fname);

	string line;
	getline(file, line);
	auto ver_b = find_nth(line, "?", 0);
	auto ver_e = find_nth(line, "?", 1);
	if((!ver_b) || (!ver_e)) {
		file.close();
		throw runtime_error("Can't open "+fname);
	}
	string version(++ver_b.begin(), ver_e.begin());
	trim(version);
	cout << version << endl;
	
	string buffer;
	getline(file, line);
	while(!file.eof()) {
		trim(line);
		buffer += line;
		getline(file, line);
	}
	file.close();
	
	auto tag_b = find_nth(buffer, "<", 0);
	auto tag_e = find_nth(buffer, ">", 0);
	string tag(++tag_b.begin(), tag_e.begin());
	auto tag_c = find_nth(buffer, "</"+tag+">", 0);
	
	return xml(tag, string(++tag_e.begin(), tag_c.begin()));
}


int main(int argc, char* argv[]) {
	if(argc != 2) {
		cout << "Usage: xml file.xml" << endl;
		return 1;
	}
	try {
		xml XML(parse(argv[1]));
		XML.print();
	} catch(runtime_error& e) {
		cout << e.what() << endl;
	}
	return 0;
}
